import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    navbar: {
      logo: './assets/logo.png',
      brandName: 'delegram',
      profile: 'Suzon'
    },
    header: {
      background: {
        backgroundImage: 'url("http://i68.tinypic.com/3022uc9.jpg")',
        backgroundSize: 'cover',
        height: '350px'
      },
      title: 'WELCOME TO DELEGRAM'
    },
    category: {
      frutisAndVeg: {
        image: 'http://i49.tinypic.com/2u7bv5f.jpg',
        title: 'Fruits and Vegetables'
      },
      meatAndFish: {
        image: 'http://i41.tinypic.com/rbhdhd.jpg',
        title: 'Meat and Fish'
      },
      cooking: {
        image: 'http://i49.tinypic.com/10ejv60.jpg',
        title: 'Cooking'
      },
      beverages: {
        image: 'http://i42.tinypic.com/25q3yvm.jpg',
        title: 'Beverages'
      },
      homeAndCleaning: {
        image: 'http://i45.tinypic.com/t9ffkm.jpg',
        title: 'Home and Cleaning'
      },
      pestControl: {
        image: 'http://i49.tinypic.com/10ejv60.jpg',
        title: 'Cooking'
      }
    },
    footer: {
      copyright: 'Deligram © 2015',
      about: 'Deligram is the largest ecommerce platform in Bangladesh focused on mass population living in the rural areas',
      streetAddress: 'Mirpur 10, Dhaka, Bangladesh',
      contact: '01714242889',
      email: 'suzon@deligram.com',
      social: {
        facebook: 'facebook.com',
        twitter: 'twitter.com'
      }
    }
  }
})
