export default {
  data () {
    return {
       brand: this.$store.state.navbar.brandName,
       user: this.$store.state.navbar.profile,
    }
  }
}