import VueAwesomeSwiper from 'vue-awesome-swiper'
import { swiper, swiperSlide } from 'vue-awesome-swiper'
export default {
  data() {
      return {
        background: this.$store.state.header.background,
        swiperOption: {
          pagination: '.swiper-pagination',
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          slidesPerView: 1,
          paginationClickable: true,
          spaceBetween: 30,
          loop: true,
          // swiper callbacks
          onTransitionStart(swiper){
            console.log(swiper)
          },
        }
      }
    },
    // you can find current swiper instance object like this, while the notNextTick property value must be true
    computed: {
      swiper() {
        return this.$refs.mySwiper.swiper
      }
    },
    mounted() {
      // you can use current swiper instance object to do something(swiper methods)
      console.log('this is current swiper instance object', this.swiper)
      this.swiper.slideTo(3, 1000, false)
    },
  components: {
    swiper,
    swiperSlide
  }
  
}