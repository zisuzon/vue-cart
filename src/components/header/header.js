export default {
  data() {
      return {
        background: this.$store.state.header.background,        
        title: this.$store.state.header.title,        
      }
    },  
}