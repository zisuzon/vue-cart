export default {
  data () {
    return {
      copyright: this.$store.state.footer.copyright,
      about: this.$store.state.footer.about,
      address: this.$store.state.footer.streetAddress,
      contact: this.$store.state.footer.contact,
      email: this.$store.state.footer.email
    }
  }
}